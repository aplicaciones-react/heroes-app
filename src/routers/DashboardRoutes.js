import React from 'react'
import { Navbar } from '../components/ui/Nabvar';
import { Switch, Route, Redirect } from 'react-router-dom';

import { MarvelScreen } from '../components/marvel/MarvelScreen';
import { HeroeScreen } from '../components/heroes/HeroeScreen';
import { DcScreen } from '../components/dc/DcScreen';
import { SearchScreen } from '../components/search/SearchScreen';

export const DashboardRoutes = () => {
    return (
        <>

            <Navbar />

            <div className="container mt-2">

                <Switch>
                    <Route exact path="/marvel" component={ MarvelScreen } />
                    <Route exact path="/dc/" component={ DcScreen } />
                    <Route exact path="/search" component={ SearchScreen } />
                    <Route exact path="/hero/:heroeId" component={ HeroeScreen } />
                    <Redirect to="/marvel" />
                </Switch>

            </div>

        </>
    )
}
