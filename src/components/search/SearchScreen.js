import React, { useMemo } from 'react';
import queryString from 'query-string';
import { useLocation } from 'react-router-dom';
// import { heroes } from '../../data/heroes';
import { useForm } from '../../hooks/useForm';
import { HeroCard } from '../heroes/HeroCard';
import { getHeroesByName } from '../../selectors/getHeroesByName';

export const SearchScreen = ({history}) => {

    //Obtener el Location
    const location = useLocation();

    const { q = ''} = queryString.parse(location.search);//q='' se usa ya que por defecto viene undefine

    
    const [formValues, handleInputChange] = useForm({
        searchText: q
    });
    
    const { searchText } = formValues;
    

    const heroesFilter = useMemo(() => getHeroesByName(q), [q]);
    // const heroesFilter = getHeroesByName(searchText);

    const handleSearch = (e) =>{
        e.preventDefault();
        history.push(`?q=${searchText}`);
    }

    return (
        <div>
            <h1>Search Screen</h1>
            <hr/>

            <div className="row">

                <div className="col-5">
                    <h4>Search Form</h4>
                    <hr/>

                    <form onSubmit={handleSearch}>
                        <input 
                            type="text"
                            placeholder="Find your Hero"
                            className = "form-control"
                            name="searchText"
                            autoComplete="off"
                            value={ searchText }
                            onChange={ handleInputChange }
                        />
                        <button
                            type="submit"
                            className="btn m-1 btn-block btn-outline-primary"
                        >
                            Search
                        </button>
                    </form>


                </div>

                <div className="col-7">

                    <h4>Results</h4>
                    <hr/>

                    {
                        (q==='')
                        &&
                        <div className="alert alert-info">
                            Search a Hero
                        </div>
                    }
                    
                    {
                        (q !== '' && heroesFilter.length === 0)
                        &&
                        <div className="alert alert-danger">
                            There is no a Hero with {q}
                        </div>
                    }


                    {
                        heroesFilter.map(hero => (
                            <HeroCard 
                                key={hero.id}
                                { ...hero }
                            />
                        ))
                    }

                </div>

            </div>
        </div>
    )
}
