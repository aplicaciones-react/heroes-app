import React, { useMemo } from 'react'
import { useParams, Redirect } from 'react-router-dom'
import { getHeroesById } from '../../selectors/getHeroesById';

export const HeroeScreen = ({history}) => {

    const {heroeId} = useParams(); //Hook para obtener los parametros de la url

    const hero = useMemo(() => getHeroesById(heroeId), [heroeId])
    // const hero = getHeroesById(heroeId);

    if (!hero) {
        return <Redirect to="/" />
    }

    const handleReturn = () =>{
        if (history.lenth <= 2) { //Validar si no tiene historial de navegacion
            history.push('/')
        }else{
            history.goBack();
        }
    }

    const {
        superhero,
        publisher,
        alter_ego,
        first_appearance,
        characters
    } = hero;

    return (
        <div className="row mt-5">
            <div className="col-4 animate__animated animate__fadeInLeft">
                <img 
                    src={`../assets/heroes/${heroeId}.jpg`} 
                    className="img-thumbnail" 
                    alt={superhero}
                />
            </div>

            <div className="col-8">
                <h3>{superhero}</h3>
                <ul className="list-group list-group-flush">
                    <li className="list-group-item"> <b>Alter ego: </b> { alter_ego }</li>
                    <li className="list-group-item"> <b>Publisher: </b> { publisher }</li>
                    <li className="list-group-item"> <b>First appearance: </b> { first_appearance }</li>
                </ul>

                <h5>Characters</h5>
                <p>{characters}</p>

                <button 
                    className="btn btn-outline-info"
                    onClick={handleReturn}
                >Return</button>
            </div> 
        </div>
    )
}
